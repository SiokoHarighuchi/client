const express = require('express');
const request = require('request');
const cors = require('cors');
const app = express();

app.use(cors());

app.use('/proxy', (req, res) => {
  const url = req.url.replace('/?url=', '');
  req.pipe(request(url)).pipe(res);
});

app.listen(process.env.PORT || 8080, () =>
  console.log('cors-proxy server started'));

