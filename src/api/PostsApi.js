import Base from './Base';

const ENDPOINTS = {
  fetchPosts: `feed`,
  fetchPostsByCategory: categoryId => `categories/${categoryId}/posts`,
  fetchComments: postId => `posts/${postId}/comments`,
  createComment: postId => `posts/${postId}/comments`,
  likePost: postId => `posts/${postId}/likes`,
  unlikePost: postId => `posts/${postId}/likes`,
  likeComment: commentId => `comments/${commentId}/likes`,
  unlikeComment: commentId => `comments/${commentId}/likes`,
};

export default class PostsApi extends Base {
  fetchPosts = (lastPostIndex, size = 10) =>
    this.apiClient.get(ENDPOINTS.fetchPosts,
      {
        params: {
          last: lastPostIndex,
          size,
        },
      })
      .then((resp) => resp.data.response);

  fetchPostsByCategory = (categoryId, lastPostIndex, size = 10) =>
    this.apiClient.get(ENDPOINTS.fetchPostsByCategory(categoryId),
      {
        params: {
          last: lastPostIndex,
          size,
        },
      })
      .then((resp) => resp.data.response);

  fetchComments = (postId, lastCommentIndex, size = 25) =>
    this.apiClient.get(ENDPOINTS.fetchComments(postId),
      {
        params: {
          last: lastCommentIndex,
          size,
        },
      })
      .then(resp => resp.data.response)
      .then(comments => {
        if (Array.isArray(comments)) return comments;
        return [];
      });

  createComment = (comment, postId) =>
    this.apiClient.post(ENDPOINTS.createComment(postId), {text: comment})
      .then(resp => resp.data.response);

  likePost = (postId) =>
    this.apiClient.post(ENDPOINTS.likePost(postId));

  unlikePost = (postId) =>
    this.apiClient.delete(ENDPOINTS.unlikePost(postId));

  likeComment = (commentId) =>
    this.apiClient.post(ENDPOINTS.likeComment(commentId));

  unlikeComment = (commentId) =>
    this.apiClient.delete(ENDPOINTS.unlikeComment(commentId));

}
