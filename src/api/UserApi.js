import Base from './Base';

const ENDPOINTS = {
  getSelfUserInfo: `users/self`,
};

export default class UserApi extends Base {
  getSelfUserInfo = () =>
   this.apiClient.get(ENDPOINTS.getSelfUserInfo)
    .then(resp => resp.data.response);
}