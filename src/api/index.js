import axios from 'axios';
import helper from './helper';

import AuthApi from './AuthApi';
import PostsApi from './PostsApi';
import ReferencesApi from './ReferencesApi';
import UserApi from './UserApi';

export default class API {
  constructor({baseURL}) {
    if (!baseURL) {
      throw new Error('baseURL required');
    }

    this.api = axios.create({
      baseURL,
      headers: {common: {}},
    });

    this.auth = new AuthApi({apiClient: this.api});
    this.posts = new PostsApi({apiClient: this.api});  
    this.references = new ReferencesApi({apiClient: this.api});
    this.user = new UserApi({apiClient: this.api});    
  }

  setLocale = (locale) => {
    this.api.defaults.headers.common['Locale'] = locale;
  };

  setAuthToken = (authToken) => {
    this.api.defaults.headers.common['Authorization'] = `Bearer ${authToken}`;
  };

  deleteAuthToken = () => {
    delete this.api.defaults.headers.common['Authorization'];
  };
}
