'use strict';
import { createStore , compose, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import reduxThunk from 'redux-thunk';

export default function configureStore(initialState) {
  return createStore(rootReducer, initialState, compose(
    applyMiddleware(reduxThunk)
  ));
}
