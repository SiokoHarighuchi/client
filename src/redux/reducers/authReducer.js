import {TYPES} from '../actions/authActions';
import API from '../../apiSingleton';
import locStorage from '../../localStorageSingelton';
import {Record} from 'immutable';

const SignInInitialState = Record({
  authorized: false,
});

const SignUpInitialState = Record({
  phoneVerified: false,
  smsSent: false,
  phone: '',
  tempToken: null,
  tempTokenTimeOut: 0,
  regToken: null,
});

const InitialState = Record({
  signIn: new SignInInitialState,
  signUp: new SignUpInitialState,
});

const initialState = new InitialState;

export default function (state = initialState, action) {
  switch (action.type) {
    case TYPES.SIGN_IN.AUTH_USER:
      locStorage.setItem(locStorage.KEYS.AUTH_TOKEN, action.authToken);
      API.setAuthToken(action.authToken);
      return state.setIn(['signIn', 'authorized'], true);
    case TYPES.SIGN_IN.UNAUTH_USER:
      locStorage.removeItem(locStorage.KEYS.AUTH_TOKEN);
      API.deleteAuthToken();
      return initialState;
    case TYPES.PHONE_VERIFICATION.GET_SMSCODE:
      return state.updateIn(['signUp'], signUp => signUp
        .set('smsSent', true)
        .set('phoneVerified', false)
        .set('phone', action.payload.phone)
        .set('tempToken', action.payload.tokenData.token)
        .set('tempTokenTimeOut', action.payload.tokenData.tokenTimeout)
      );
    case TYPES.PHONE_VERIFICATION.VALIDATE_SMS:
      return state.updateIn(['signUp'], signUp => signUp
        .set('phoneVerified', true)
        .set('regToken', action.payload)
      );
    case TYPES.PHONE_VERIFICATION.CHANGE_TIMEOUT:
      return state.updateIn(['signUp'], signUp => signUp
        .set('tempTokenTimeOut', action.payload));
    case TYPES.PHONE_VERIFICATION.EXPIRE_SMSCODE:
      return state.updateIn(['signUp'], signUp => signUp
        .set('smsSent', false)
        .set('phone', '')
        .set('tempToken', null)
        .set('tempTokenTimeOut', 0)
      );
    case TYPES.REGISTRATION.SET_REG_TOKEN_TO_STATE:
      return state.updateIn(['signUp'], signUp => signUp
        .set('phoneVerified', true)
        .set('regToken', action.payload)
      );
    case TYPES.REGISTRATION.DELETE_SIGNUP_CREDENTIALS:
      return state.set('signUp', new SignUpInitialState);
  }
  return state;
}