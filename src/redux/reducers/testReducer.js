'use strict';
import {Record, Map} from 'immutable';
import * as ACTIONS from '../actions/testActions';

const InitialState = Record({
  bool: false,
  items: Map({})
});

const initialState = new InitialState;

export default function testReducer(state=initialState, action) {
  switch(action.type) {
    case ACTIONS.CHANGE_BOOL: {
      return state.set('bool', !state.bool);
    }
    case ACTIONS.ADD_ITEM: {
      const obj = {hihi: 'hi'};
      return state.update('items', items => items.set(action.payload, {hello: action.payload}) )
    }

    case ACTIONS.ADD_BUNCH_ITEMS: {
      const items = [
        {hello: '11'},{hello: '16'},{hello: '13'},{hello: '14'}
      ];

      const mapItems = items.reduce((mapItems, objItem) => 
        mapItems.set(objItem.hello, objItem), Map());

      return state.update('items', items => items.merge(mapItems))
    }
  }
  return state;
}