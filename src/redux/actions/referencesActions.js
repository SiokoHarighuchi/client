import API from '../../apiSingleton';

export const TYPES = {
  FETCH_REFERENCES: 'FETCH_REFERENCES',
};

export function fetchReferences() {
  return dispatch =>
    API.references.fetchReferences()
      .then(references => dispatch({type: TYPES.FETCH_REFERENCES, references}));
}
