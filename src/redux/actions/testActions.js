'use strict';

export const CHANGE_BOOL = 'CHANGE_BOOL';
export const ADD_ITEM = 'ADD_ITEM';
export const ADD_BUNCH_ITEMS = 'ADD_BUNCH_ITEMS';

export function changeBool() {
  return {type: CHANGE_BOOL}
}

export function addItem(item) {
  return {type: ADD_ITEM, payload: item}
}

export function addBunchItems() {
  return {type: ADD_BUNCH_ITEMS};
}