import API from '../../apiSingleton';

export const TYPES = {
  SET_PRIMARY_USER_INFO: 'set_user_info_to_state',
  GET_SELF_USER_INFO: 'get_self_user_info',
};


export function setPrimaryUser(userInfo) {
  return {
    type: TYPES.SET_PRIMARY_USER_INFO,
    userInfo,
  };
}

export function getSelfUserInfo() {
  return dispatch =>  
    API.user.getSelfUserInfo()
      .then(user =>
        dispatch({type: TYPES.GET_SELF_USER_INFO, user}));
}