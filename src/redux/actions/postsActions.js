import API from '../../apiSingleton';
import {BEGIN, COMMIT, REVERT} from 'redux-optimist';
import {generateId} from '../../utils/common';
import moment from 'moment';

export const TYPES = {
  START_POSTS_FETCHING: 'START_POSTS_FETCHING',
  STOP_POSTS_FETCHING: 'STOP_POSTS_FETCHING',
  FETCH_POSTS: 'FETCH_POSTS',
  FETCH_POSTS_BY_CATEGORY: 'FETCH_POSTS_BY_CATEGORY',
  CLEAR_POSTS: 'CLEAR_POSTS',
  FETCH_COMMENTS: 'FETCH_COMMENTS',
  CREATE_COMMENT: 'CREATE_COMMENT',
  CREATE_COMMENT_COMPLETE: 'CREATE_COMMENT_COMPLETE',
  CREATE_COMMENT_FAILED: 'CREATE_COMMENT_FAILED',
  LIKE_POST: 'LIKE_POST',
  LIKE_POST_COMPLETE: 'LIKE_POST_COMPLETE',
  LIKE_POST_FAILED: 'LIKE_POST_FAILED',
  UNLIKE_POST: 'UNLIKE_POST',
  UNLIKE_POST_COMPLETE: 'UNLIKE_POST_COMPLETE',
  UNLIKE_POST_FAILED: 'UNLIKE_POST_FAILED',
  LIKE_COMMENT: 'LIKE_COMMENT',
  LIKE_COMMENT_COMPLETE: 'LIKE_COMMENT_COMPLETE',
  LIKE_COMMENT_FAILED: 'LIKE_COMMENT_FAILED',
  UNLIKE_COMMENT: 'UNLIKE_COMMENT',
  UNLIKE_COMMENT_COMPLETE: 'UNLIKE_COMMENT_COMPLETE',
  UNLIKE_COMMENT_FAILED: 'UNLIKE_COMMENT_FAILED',
};

export function startFetchingPosts() {
  return {type: TYPES.START_POSTS_FETCHING};
}

export function stopFetchingPosts() {
  return {type: TYPES.STOP_POSTS_FETCHING};
}

export function fetchPosts() {
  return (dispatch, getState) => {
    dispatch(startFetchingPosts());

    API.posts.fetchPosts(getState().feed.lastPostId)
      .then(posts => {
        dispatch({type: TYPES.FETCH_POSTS, posts});
      })
      .then(() => dispatch(stopFetchingPosts()))
      .catch(() => dispatch(stopFetchingPosts()));
  };
}

export function fetchPostsByCategory(categoryId) {
  return (dispatch, getState) => {
    dispatch(startFetchingPosts());

    API.posts.fetchPostsByCategory(categoryId, getState().feed.lastPostId)
      .then(posts => {
        dispatch({type: TYPES.FETCH_POSTS, posts});
      })
      .then(() => dispatch(stopFetchingPosts()))
      .catch(() => dispatch(stopFetchingPosts()));
  };
}

export function clearPosts() {
  return {type: TYPES.CLEAR_POSTS};
}

export function fetchComments(postId, lastCommentId, size) {
  return dispatch =>
    API.posts.fetchComments(postId, lastCommentId, size)
      .then(comments => {
        if (comments.length === 0) return; // if all comments already have been fetched, do nothing;
        dispatch({type: TYPES.FETCH_COMMENTS, postId, comments});
      });
}

export function createComment(postId, commentData) {
  return (dispatch, getState) => {
    const transactionID = generateId();

    const tempComment = {
      text: commentData,
      id: transactionID,
      created_at: moment().unix() + 80, // todo time bug, почему то не всегда появляется последней в списке.
      user: getState().user,
      isCreating: true,
    };

    dispatch({
      type: TYPES.CREATE_COMMENT,
      postId, tempComment,
      optimist: {type: BEGIN, id: transactionID},
    });

    return API.posts.createComment(commentData, postId)
      .then(comment => dispatch({
        type: TYPES.CREATE_COMMENT_COMPLETE,
        tempCommentId: transactionID,
        postId, comment,
        optimist: {type: COMMIT, id: transactionID},
      }))
      .catch((err) => dispatch({
        type: TYPES.CREATE_COMMENT_FAILED,
        tempCommentId: transactionID,
        postId,
        optimist: {type: REVERT, id: transactionID},
      }));
  };
}

export function likePost(postId) {
  return dispatch => {
    const transactionID = generateId();

    dispatch({
      type: TYPES.LIKE_POST,
      postId,
      optimist: {type: BEGIN, id: transactionID},
    });

    API.posts.likePost(postId)
      .then(() => dispatch({
        type: TYPES.LIKE_POST_COMPLETE,
        optimist: {type: COMMIT, id: transactionID},
      }))
      .catch(() =>
        dispatch({
          type: TYPES.LIKE_POST_FAILED,
          postId,
          optimist: {type: REVERT, id: transactionID},
        }));
  };
}

export function unlikePost(postId) {
  return dispatch => {
    const transactionID = generateId();

    dispatch({
      type: TYPES.UNLIKE_POST,
      postId,
      optimist: {type: BEGIN, id: transactionID},
    });

    return API.posts.unlikePost(postId)
      .then(() => dispatch({
        type: TYPES.UNLIKE_POST_COMPLETE,
        optimist: {type: COMMIT, id: transactionID},
      }))
      .catch(() => dispatch({
        type: TYPES.UNLIKE_POST_FAILED,
        postId,
        optimist: {type: REVERT, id: transactionID},
      }));
  };
}

export function likeComment(postId, commentId) {
  return dispatch => {
    const transactionID = generateId();

    dispatch({
      type: TYPES.LIKE_COMMENT,
      postId, commentId,
      optimist: {type: BEGIN, id: transactionID},
    });

    API.posts.likeComment(commentId)
      .then(() => dispatch({
        type: TYPES.LIKE_COMMENT_COMPLETE,
        optimist: {type: COMMIT, id: transactionID},
      }))
      .catch(() => dispatch({
        type: TYPES.LIKE_COMMENT_FAILED,
        postId, commentId,
        optimist: {type: REVERT, id: transactionID},
      }));
  };
}

export function unlikeComment(postId, commentId) {
  return dispatch => {
    const transactionID = generateId();

    dispatch({
      type: TYPES.UNLIKE_COMMENT,
      postId, commentId,
      optimist: {type: BEGIN, id: transactionID},
    });

    API.posts.unlikeComment(commentId)
      .then(() => dispatch({
        type: TYPES.UNLIKE_COMMENT_COMPLETE,
        optimist: {type: COMMIT, id: transactionID},
      }))
      .catch(() => dispatch({
        type: TYPES.UNLIKE_COMMENT_FAILED,
        postId, commentId,
        optimist: {type: REVERT, id: transactionID},
      }));
  };
}