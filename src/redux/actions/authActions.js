import API from '../../apiSingleton';

import {reverseTimer} from '../../utils/timer';
import {setPrimaryUser} from './userActions';
import {formatIso} from '../../utils/common'

export const TYPES = {
  SIGN_IN: {
    AUTH_USER: 'AUTH_USER',
    UNAUTH_USER: 'UNAUTH_USER',
  },
  PHONE_VERIFICATION: {
    GET_SMSCODE: 'GET_SMSCODE',
    VALIDATE_SMS: 'VALIDATE_SMS',
    CHANGE_TIMEOUT: 'CHANGE_TIMEOUT',
    EXPIRE_SMSCODE: 'EXPIRE_SMSCODE',
  },
  REGISTRATION: {
    SET_REG_TOKEN_TO_STATE: 'SET_REG_TOKEN_TO_STATE',
    DELETE_SIGNUP_CREDENTIALS: 'DELETE_SIGNUP_CREDENTIALS',
    SIGN_UP_USER: 'SIGN_UP_USER',
  },
};

// todo generate mock udid
const MOCK_UDID = '6PPDHGILX5N4S4IJU93C18A5XFOS9K9O';

//////////////////////SIGN IN BLOCK
export function signInUser({phone, password}) {
  return dispatch =>
    API.auth.singInUser(phone, password, MOCK_UDID)
      .then(userData => {
        dispatch(setPrimaryUser(userData.userInfo));
        dispatch({
            type: TYPES.SIGN_IN.AUTH_USER,
            authToken: userData.authToken,
          }
        );
      }); // catch in form component
}

export function signOutUser() {
  return {type: TYPES.SIGN_IN.UNAUTH_USER};
}

//////////////////////PHONE VERIFICATION BLOCK
export function getSmsCode(phone) {
  return (dispatch, getState) =>
    API.auth.getSmsCode(phone, MOCK_UDID)
      .then(tokenData => {
        // if phone is not validated we get temporary token
        if (tokenData.temp) {
          // set timer for reduce expiration  temp token time
          reverseTimer(tokenData.tokenTimeout,
            (nextTimeOut) => {
              if (getState().auth.signUp.tempTokenTimeOut === 0) return; //do nothing when we've validated sms
              dispatch(changeSmsTokenTimeout(nextTimeOut));
            },
            () => {
              dispatch(expireSmsCode());
            }
          );

          dispatch({
            type: TYPES.PHONE_VERIFICATION.GET_SMSCODE,
            payload: {phone, tokenData},
          });
        } else {
          //else we get actual registration token
          dispatch(setRegTokenToState(tokenData.token));
        }
      }); // catch in form component
}

export function validateSmsCode(code, token) {
  return (dispatch) =>
    API.auth.validateSmsCode(code, token, MOCK_UDID)
      .then(regToken => {
        dispatch({type: TYPES.PHONE_VERIFICATION.EXPIRE_SMSCODE});
        dispatch({type: TYPES.PHONE_VERIFICATION.VALIDATE_SMS, payload: regToken});
      }); // catch in form component
}

function changeSmsTokenTimeout(nextTimeout) {
  return {
    type: TYPES.PHONE_VERIFICATION.CHANGE_TIMEOUT,
    payload: nextTimeout,
  };
}

function expireSmsCode() {
  return {type: TYPES.PHONE_VERIFICATION.EXPIRE_SMSCODE};
}

export function setRegTokenToState(regToken) {
  return {
    type: TYPES.REGISTRATION.SET_REG_TOKEN_TO_STATE,
    payload: regToken,
  };
}

// REGISTRATION BLOCK
export function signUpUser(data) {
  return (dispatch, getState) => {   
    
    const sex = +data.sex || 1; // coercion to int
    const birthday = formatIso(data.birthday); // format data
    const regToken = getState().auth.signUp.regToken;

    const requestData = {...data, token: regToken, sex, birthday};

    return API.auth.signUpUser(requestData)
      .then((userData) => {
          dispatch(setPrimaryUser(userData.userInfo));
          dispatch({type: TYPES.REGISTRATION.DELETE_SIGNUP_CREDENTIALS}); // delete creds on reg success
          dispatch({type: TYPES.SIGN_IN.AUTH_USER, authToken: userData.authToken}); // auth user on reg success
        }
      );
  };
}