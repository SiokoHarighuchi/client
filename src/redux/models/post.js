import {Record, Map} from 'immutable';

const Post = Record({
  id: null,
  text: null,
  created_at: null,
  updated_at: null,
  user: null,
  attachments: Map(),
  comments: Map(),
  comments_count: 0,
  lastCommentId: null,
  likes: 0,
  commented: 0,
  liked: 0,
  is_fav: 0,
  category: null
});

export default Post;