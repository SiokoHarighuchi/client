import {Record, Map} from 'immutable';

const Comment = Record({
  id: null,
  text: null,
  created_at: null,
  updated_at: null,
  user: null,
  likes: 0,
  attachments: Map(),
  liked: 0,
  isCreating: false, // my field
});

export default Comment;