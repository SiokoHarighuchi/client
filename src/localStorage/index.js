export default class LocalStorage {
  constructor() {
    this.KEYS = {
      AUTH_TOKEN: 'auth_token',
      REFERENCES: 'references',
    };
  } 

  setItem = (key, item) => localStorage.setItem(key, JSON.stringify(item));
  getItem = (key) => JSON.parse(localStorage.getItem(key));
  removeItem = (key) => localStorage.removeItem(key);
}