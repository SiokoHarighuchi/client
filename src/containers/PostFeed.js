import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {fetchPosts, fetchPostsByCategory, clearPosts} from '../redux/actions/postsActions';

import Waypoint from 'react-waypoint';
import PostList from '../components/posts/PostList';
import CircularProgressWrapper from '../components/common/CircularProgress';

@connect(state => ({feed: state.feed}), {fetchPosts, fetchPostsByCategory, clearPosts})
export default class PostFeedContainer extends Component {
  static propTypes = {
    feed: PropTypes.shape({
      posts: PropTypes.object.isRequired,
      isFetching: PropTypes.bool.isRequired,
    }),
    params: PropTypes.object,
    fetchPosts: PropTypes.func.isRequired,
    fetchPostsByCategory: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.fetch();
  }

  componentWillReceiveProps(nextProps) {
    //clear store on entering new category
    if (this.props.params.categoryId !== nextProps.params.categoryId) {
      this.props.clearPosts();
    }
  }

  componentWillUnmount() {
    this.props.clearPosts();
  }

  fetch = () => {
    if (this.props.params.categoryId) {
      this.props.fetchPostsByCategory(this.props.params.categoryId);
    } else {
      this.props.fetchPosts();
    }
  }

  handleWaypointEnter = () => {
    this.fetch();
  }

  render() {
    const posts = this.props.feed.posts.toList().sortBy(post => post.created_at).reverse();

    return (
      <div>
        <PostList posts={posts}/>
        {this.props.feed.isFetching ? <CircularProgressWrapper size={0.8}/> : null}
        {this.props.feed.isFetching ? null :
          <Waypoint
            onEnter={this.handleWaypointEnter}
            threshold={2.0}
          />
        }
      </div>
    );
  }
}



