import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {getSelfUserInfo} from '../redux/actions/userActions';

import UserProfile from '../components/UserProfile/UserProfile';

@connect(state => ({user: state.user, references: state.references}), {getSelfUserInfo})
export default class UserProfileContainer extends Component {
  static propTypes = {
    user: PropTypes.object,
    getSelfUserInfo: PropTypes.func.isRequired,
    references: PropTypes.object,
  }

  componentWillMount() {
    this.props.getSelfUserInfo();
  }

  render() {
    if (!this.props.user.birthday) return <div>loading...</div>; // todo придумать более умную проверку на получение фулл профиля

    let cityName = '';
    // todo хз может вынести в отдельный файл с селекторами
    if (this.props.references.cities.length > 0) {
      cityName = this.props.references.cities
        .find(c => c.id === this.props.user.city).name;
    }

    const user = Object.assign(this.props.user, {cityName});

    return (
      <UserProfile user={user}/>
    );
  }
}