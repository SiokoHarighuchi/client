import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {likeComment, unlikeComment} from '../redux/actions/postsActions';

import Comment from '../components/CommentBox/Comment';

@connect(null, {likeComment, unlikeComment})
export default class CommentContainer extends Component {
  static propTypes = {
    comment: PropTypes.object.isRequired,
    postId: PropTypes.number.isRequired,
    likeComment: PropTypes.func,
    unlikeComment: PropTypes.func,
  };
  
  handleLikeComment = () => {
    if (!this.props.comment.liked) {
      this.props.likeComment(this.props.postId, this.props.comment.id);
    } else {
      this.props.unlikeComment(this.props.postId, this.props.comment.id);
    }
  };
  
  render() {
    return (
      <Comment 
        comment={this.props.comment}
        onLikeComment={this.handleLikeComment}
      />
    );
  }
}