export function reverseTimer(timeOut, cb, onEnd) {
  let res = timeOut;
  cb(res);

  const timerId = setInterval(() => {
    --res;
    if (res === 0) {
      clearInterval(timerId);
      onEnd();
      return;
    }
    cb(res);
  }, 1000);
}

