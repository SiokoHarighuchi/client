import React, {Component} from 'react';
import {connect} from 'react-redux';
import styles from './Header.scss';

import {Link} from 'react-router';
import {ROUTES} from '../../routes';

import MaxWidthWrapper from '../common/MaxWidthWrapper/MaxWidthWrapper';
import HeaderAuthorized from './HeaderAuthorized/HeaderAuthorized';
import HeaderUnauthorized from './HeaderUnauthorized/HeaderUnauthorized';

// todo refactor and container
@connect(state => ({authorized: state.auth.signIn.authorized}))
export default class Header extends Component {
  renderAuthHeader() {
    // return this.props.authorized ? <HeaderAuthorized pathname={this.props.pathname}/> : <HeaderUnauthorized />
  }

  render() {
    return (
      <div className={styles.header}>
        <MaxWidthWrapper>
          <div className={styles.box}>
            <div className={"col-sm-3 hidden-xs-down " + styles.myCol}>
              <Link to={ROUTES.HOME}>
                <img className={styles.logo} src={require('./img/logo.png')}/>
              </Link>
            </div>
            <div className={"col-xs-12 col-sm-9 " + styles.myCol}>
              {this.renderAuthHeader()}
            </div>
          </div>
        </MaxWidthWrapper>
      </div>
    );
  }
}
