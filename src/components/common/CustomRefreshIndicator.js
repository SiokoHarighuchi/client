import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const CustomRefreshIndicator = () => {
  return (
    <div>
      <RefreshIndicator
        size={40}
        left={10}
        top={0}
        status="loading"
      />
    </div>
  );
};

export default CustomRefreshIndicator;