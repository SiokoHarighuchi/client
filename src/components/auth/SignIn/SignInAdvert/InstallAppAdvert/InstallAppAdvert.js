import React, {Component, PropTypes} from 'react';
import styles from './InstallAppAdvert.scss';

const InstallAppAdvert = (props) => (
  <div className={styles.box}>
    <img className={styles.img} src={props.imgSrc} alt="our app"/>
    <a href={props.storeLink} target="_blank">{props.storeLinkText}</a>
  </div>
);

InstallAppAdvert.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  storeLink: PropTypes.string.isRequired,
  storeLinkText: PropTypes.string.isRequired,
};

export default InstallAppAdvert;