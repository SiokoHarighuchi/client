import React, {Component, PropTypes} from 'react';
import styles from './RegistrationForm.scss';
import {reduxForm} from 'redux-form';
import registrationValidation from './RegistrationValidation';

import placeHolderImg from './img/placeholder_128.png';
import FormErrorAlert from '../../../common/FormErrorAlert';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import DatePickerWrapper from '../../../common/DatePickerWrapper';
import SelectFieldWrapper from '../../../common/SelectFieldWrapper';
import MenuItem from 'material-ui/MenuItem';
import {RadioButtonGroup, RadioButton} from 'material-ui/RadioButton';
import Dropzone from 'react-dropzone';

const FORM_OPTIONS = {
  form: 'registration',
  fields: ['name', 'email', 'img', 'birthday', 'city', 'sex', 'password', 'passwordConfirm'],
  validate: registrationValidation,
};

// todo container
@reduxForm(FORM_OPTIONS, state => ({cities: state.references.cities}))
export default class RegistrationForm extends Component {
  static propTypes = {
    signUpUser: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    error: PropTypes.string,
    fields: PropTypes.object,
    cities: PropTypes.array,
  };

  constructor(props) {
    super(props);

    const maxBirthDate = new Date();
    maxBirthDate.setFullYear(maxBirthDate.getFullYear());
    maxBirthDate.setHours(0, 0, 0, 0);

    this.state = {
      cityList: [],
      avatar: null,
      maxBirthDate,
    };
  }

  handleFormSubmit(formData) {   
    return this.props.signUpUser(formData)
      // server validation
      .catch(err => Promise.reject({_error: err.message}));
  }

  renderAvatarDropzone(img) {
    const imgSrc = this.state.avatar ? this.state.avatar.preview : placeHolderImg;

    return (
      <Dropzone
        className={styles.avatarDropBox}
        multiple={false}
        onDrop={(filesToUpload, e) => {
          this.setState({avatar: filesToUpload[0]});
          img.onChange(filesToUpload[0]);
          }
        }
      >
        <div>выберите аватар:</div>
        <div className={styles.avatar} style={{backgroundImage: `url(${imgSrc})`}}></div>
      </Dropzone>
    );
  }

  renderCityListItems() {
    const cities = this.props.cities;

    if (cities.length > 0) {
      return cities.map(city =>
        <MenuItem key={city.id} value={city.id} primaryText={city.name}/>);
    } else {
      return <MenuItem value="" primaryText="Города не загружены"/>;
    }
  }

  render() {
    const {
      handleSubmit,
      submitting,
      error,
      fields: {
        name, email, img: {value: _, ...img}, birthday,
        city, sex, password, passwordConfirm,
      },
    } = this.props;

    return (
      <Paper zDepth={1} className={styles.box}>
        <form
          className={styles.formBox}
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <h2>Регистрация</h2>
          <TextField
            {...name}
            className={styles.textField}
            hintText="Имя"
            floatingLabelText="Имя"
            errorText={name.touched && name.error ? name.error : ''}
          />
          <TextField
            {...email}
            className={styles.textField}
            hintText="email"
            floatingLabelText="email"
            errorText={email.touched && email.error ? email.error : ''}
          />
          <fieldset className={styles.formGroupBox}>
            <DatePickerWrapper 
              {...birthday}
              hintText="дата рождения"
              maxDate={this.state.maxBirthDate}
              errorText={birthday.touched && birthday.error ? birthday.error : ''}
            />
          </fieldset>
          <fieldset className={styles.formGroupBox}>
            <RadioButtonGroup className={styles.sexBox} defaultSelected="1" name="sex"  {...sex}>
              <RadioButton
                className={styles.radio}
                value="1"
                label="мужчина"
              />
              <RadioButton
                className={styles.radio}
                value="2"
                label="женщина"
              />
            </RadioButtonGroup>
          </fieldset>
          <SelectFieldWrapper
            {...city}
            hintText="Город"
            floatingLabelText="Город"
            maxHeight={200}
            errorText={city.touched && city.error ? city.error : ''}
          >
            {this.renderCityListItems()}
          </SelectFieldWrapper>
          <fieldset className={styles.formGroupBox}>
            {this.renderAvatarDropzone(img)}
          </fieldset>
          <TextField
            {...password}
            type="password"
            className={styles.textField}
            hintText="Пароль"
            floatingLabelText="Пароль"
            errorText={password.touched && password.error ? password.error : ''}
          />
          <TextField
            {...passwordConfirm}
            type="password"
            className={styles.textField}
            hintText="Подтвердите пароль"
            floatingLabelText="Подтвердите пароль"
            errorText={passwordConfirm.touched && passwordConfirm.error
              ? passwordConfirm.error : ''}
          />
          <fieldset className={styles.formGroupBox}>
            <RaisedButton
              primary={true}
              label="отправить"
              type="submit"
              disabled={submitting}
            />
          </fieldset>
          {error ? <FormErrorAlert error={error}/> : ''}
        </form>
      </Paper>
    );
  }
}