import * as v from '../../../../utils/validation';

const registrationValidation = v.createValidator({
  name: [v.required],
  email: [v.required, v.email],
  birthday: [v.required],
  city: [v.required],
  password: [v.required, v.minLength(6)],
  passwordConfirm: [v.required, v.minLength(6), v.match('password')],
});

export default registrationValidation;