import React, {Component, PropTypes} from 'react';

import styles from './PhoneVerificationForm.scss';
import {reduxForm} from 'redux-form';

import FormErrorAlert from '../../../common/FormErrorAlert';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
//todo container and component 
const FORM_OPTIONS = {
  form: 'phoneVerification',
  fields: ['phone', 'smsCode'],
};
@reduxForm(FORM_OPTIONS, null)
export default class PhoneVerificationForm extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    getSmsCode: PropTypes.func.isRequired,
    validateSmsCode: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    error: PropTypes.string,
    fields: PropTypes.object,
  };
  
  handleFormSubmit({phone, smsCode}) {
    if (!this.props.auth.signUp.tempToken) {
      return this.props.getSmsCode(phone)
        // server validation
        .catch(err => Promise.reject({_error: err.message}));
    } else {
      return this.props.validateSmsCode(smsCode, this.props.auth.signUp.tempToken)
        // server validation
        .catch(err => Promise.reject({_error: err.message}));
    }
  }

  renderTimeOut() {
    if (this.props.auth.signUp.smsSent) {
      return (
        <div>
          получить смс повторно через <span className="font-italic">
          {this.props.auth.signUp.tempTokenTimeOut}</span>
        </div>
      );
    }
  }

  render() {
    const {handleSubmit, submitting, error, fields: {phone, smsCode}} = this.props;

    return (
      <Paper zDepth={1} className={styles.box}>
        <h2>Верификация телефона</h2>
        <div className={styles.text}>Для продолжения регистрации подтвердите свой телефон</div>
        <div className="col-md-4">
          <form
            className={styles.formBox}
            onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <fieldset className={styles.formGroup}>
              <TextField
                {...phone}
                className={styles.textField}
                disabled={submitting || this.props.auth.signUp.smsSent}
                hintText="Телефон"
                floatingLabelText="Телефон"
                value={phone.value || this.props.auth.signUp.phone}
              />
            </fieldset>
            <fieldset className={this.props.auth.signUp.smsSent ? '' : styles.smsFormGroup_hide }>
              <TextField
                {...smsCode}
                className={styles.textField}
                disabled={submitting}
                hintText="СМС Код"
                floatingLabelText="СМС Код"
              />
            </fieldset>
            <fieldset className={styles.formGroup}>
              <RaisedButton
                className={styles.button}
                primary={true}
                type="submit"
                disabled={submitting}
                label={this.props.auth.signUp.smsSent ? 'ввести код' : 'получить код'}
              />
            </fieldset>
            <div className={styles.formGroup}>
              {error ? <FormErrorAlert error={error} /> : ''}
              {this.renderTimeOut()}
            </div>
          </form>
        </div>
      </Paper>
    );
  }
}

