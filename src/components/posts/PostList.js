import React, {PropTypes} from 'react';
import styles from './PostList.scss';

import LazyLoad from 'react-lazyload';
import Post from '../../containers/Post';

const PostList = (props) => {
  const postItems = props.posts.map(post => (
      <li className={styles.postItem} key={post.id}>
        <LazyLoad height={props.lazyLoadHeight} offset={props.lazyLoadOffset}>
          <Post
            post={post}
          />
        </LazyLoad>
      </li>
    )
  );

  return (
    <ul className={styles.postList}>
      {postItems}
    </ul>
  );
};

PostList.propTypes = {
  posts: PropTypes.object.isRequired,
  lazyLoadHeight: PropTypes.number,
  lazyLoadOffset: PropTypes.number,
};

export default PostList;