import React, {PropTypes} from 'react';
import styles from './CommentExpandButton.scss';

import FlatButton from 'material-ui/FlatButton';

const CommentExpandButton = (props) => {
  let message = '';
  // todo refactor и хз куда запихнуть, по идее здесь, но может в контейнер?
  if (!props.expanded && props.fetchedCommentsCount < props.allCommentsCount) {
    message = `показать последние ${props.fetchedCommentsCount} коментариев из ${props.allCommentsCount}`;
  } else if (props.expanded && props.fetchedCommentsCount < props.allCommentsCount
    && props.allCommentsCount - props.fetchedCommentsCount > props.fetchSize) {
    message = `показать последние ${props.fetchSize + props.fetchedCommentsCount} коментариев из ${props.allCommentsCount}`;
  } else if (props.expanded && props.fetchedCommentsCount < props.allCommentsCount) {
    message = `показать все ${props.allCommentsCount} комментариев`;
  }
  else if (!props.expanded) {
    message = `показать все ${props.allCommentsCount} комментариев`;
  } else {
    message = `свернуть комментарии`;
  }

  return (
    <div className={styles.box}>
      <FlatButton
        onClick={props.onClick}
        backgroundColor="rgba(143, 195, 245, 0.26)"
        style={{width: '90%'}}
      >
        {message}
      </FlatButton>
    </div>
  );
};

CommentExpandButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  expanded: PropTypes.bool.isRequired,
  fetchedCommentsCount: PropTypes.number.isRequired,
  allCommentsCount: PropTypes.number.isRequired,
  fetchSize: PropTypes.number.isRequired,
};

export default CommentExpandButton;