import React, {Component, PropTypes} from 'react';
import styles from './CommentList.scss';

import Comment from '../../containers/Comment';
import Divider from 'material-ui/Divider';

export default class CommentList extends Component {
  static propTypes = {
    comments: PropTypes.object.isRequired,  
    postId: PropTypes.number.isRequired,
  };

  renderComments() {
    return this.props.comments.map(comment => (
      <li key={comment.id}>
        <Comment
          postId={this.props.postId}
          comment={comment}
        />
        <Divider />
      </li>
    ));
  }

  render() {
    return (
      <ul className={styles.commentList}>
        {this.renderComments()}
      </ul>
    );
  }
}