import React, {Component} from 'react';
import Paper from 'material-ui/Paper';
import {connect} from 'react-redux';

import {spring} from 'react-motion';

import * as actions from '../redux/actions/testActions';
// test immutable not for prod
class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemNum: 0,
    };
  }

  switchBool() {
    this.props.changeBool();
  }

  addItem() {
    this.props.addItem(this.state.itemNum);
    this.setState({itemNum: this.state.itemNum + 1});
  }

  addBunchItems() {
    this.props.addBunchItems();
  }

  renderItems() {
    if (this.props.test.items) {
      return this.props.test.items.toList().map(item => {
        return <li>{item.hello}</li>;
      });
    }
  }

  render() {
    return (
      <Paper>
        <spring defaultValue={0} endValue={360}>
          {val => <div>{val}</div>}
        </spring>

        <button onClick={this.switchBool.bind(this)}>switch Bool</button>
        <button onClick={this.addItem.bind(this)}> add Item</button>
        <button onClick={this.addBunchItems.bind(this)}> add bunch</button>
        <div><h1>{this.props.test.bool ? 'hello' : 'bye'}</h1></div>
        <ul>
          {this.renderItems()}
        </ul>
      </Paper>
    );
  }
}

function mapStateToProps(state) {
  // console.log(state.test);
  return {
    test: state.test,
  };
}

export default connect(mapStateToProps, actions)(Test);