import React, {Component, PropTypes} from 'react';
import styles from './BaseContentLayout.scss';

const BaseContentLayout = (props) => {
  const hideAsideClasses = !props.hideMainContent ?
    "hidden-sm-down " : "col-sm-12 ";
  const hideMainContentClasses = props.hideMainContent ?
    "hidden-sm-down " : "col-sm-12 ";
// todo refactor strings to ``
  return (
    <div className={"container-fluid " + styles.box}>
      <div className={"col-md-3 " + hideAsideClasses + styles.myColAside}>
          {props.aside || ''}
      </div>
      <div className={"col-md-9 " + hideMainContentClasses + styles.myColMain}>
          {props.mainContent || ''}
      </div>
    </div>
  );
};

BaseContentLayout.propTypes = {
  aside: PropTypes.element,
  mainContent: PropTypes.element,
  hideMainContent: PropTypes.bool,
};

export default BaseContentLayout;