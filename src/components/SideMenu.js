import React, {Component, PropTypes} from 'react';
import styles from './SideMenu.scss';

import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';

import {ROUTES} from '../routes';
//todo container!
export default class SideMenu extends Component {
  static propTypes = {
    onExitTouch: PropTypes.func.isRequired,
    onMenuValueChange: PropTypes.func.isRequired,
  }  

  render() {
    return (
      <Paper className={styles.box}>
        <Menu
          className={styles.menu}
          autoWidth={false}          
          onChange={this.props.onMenuValueChange}
        >
          <MenuItem
            className={styles.menuItem}
            primaryText="лента"
            leftIcon={<FontIcon className="fa fa-file-text" />}
            value={ROUTES.FEED}
          />
          <MenuItem
            primaryText="профиль"
            leftIcon={<FontIcon className="fa fa-user" />}
            value={ROUTES.USER_PROFILE}
          />
          <MenuItem
            primaryText="сообщения"
            leftIcon={<FontIcon className="fa fa-comments-o" />}
            value={ROUTES.MESSAGES}

          />
          <MenuItem
            primaryText="выйти"
            leftIcon={<FontIcon className="fa fa-sign-out" />}
            onTouchTap={this.props.onExitTouch}
          />
        </Menu>
      </Paper>
    );
  }
}

