import React, {Component} from 'react';
import styles from './Footer.scss';
import logoImg  from './img/kode-logo.png';

import MaxWidthWrapper from '../common/MaxWidthWrapper/MaxWidthWrapper';

const Footer = () => (
  <div className={styles.footer}>
    <MaxWidthWrapper>
      <div className={styles.box}>
        <img className={styles.logo} src={logoImg} alt="appkode.ru"/>
      </div>
    </MaxWidthWrapper>
  </div>
);

export default Footer;