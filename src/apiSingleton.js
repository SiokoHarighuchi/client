import apiFactory from './api';

const api = new apiFactory({
  baseURL: 'http://yol-dev.cloudapp.net/api/',
});

export default api;